package com.example.bt2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;

@SpringBootApplication
public class Bt2Application {

	public static int count ;

	public static int countEndThread = 0  ;

	public static void main(String[] args) {
		SpringApplication.run(Bt2Application.class, args);

	}
	public static boolean check(int age) {
		if (age == 21) {
			return true ;
		}else {
			return false ;
		}
	}
	public static synchronized void add(int value){
		count += value;
	}
	public static void write() {
		FileOutputStream fileOutputStream = null;
		BufferedWriter bufferedWriter = null;

		try {
			fileOutputStream = new FileOutputStream("res.txt");
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));
			if(count >2) {
				bufferedWriter.write("true");
			}else {
				bufferedWriter.write("false");
			}
		} catch (FileNotFoundException ex) {

		} catch (IOException ex) {

		} finally {
			try {
				bufferedWriter.close();
				fileOutputStream.close();
			} catch (IOException ex) {

			}
		}
	}

}
