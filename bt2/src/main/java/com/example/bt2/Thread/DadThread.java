package com.example.bt2.Thread;

import com.example.bt2.Bt2Application;
import com.example.bt2.model.ShareData;
import com.example.bt2.service.TestService;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.concurrent.locks.ReentrantLock;

public class DadThread extends Thread{

    private final Logger logger = LoggerFactory.getLogger(TestService.class);

    private ShareData shareData ;
    private String name ;
    private int age ;

    public DadThread(String name , int age , ShareData shareData) {
        this.name = name;
        this.age = age ;
        this.shareData = shareData ;
    }
    @Override
    public void run() {
//        System.out.println("DadThread "+shareData.getName()+" Run");
        logger.info("DadThread "+shareData.getName()+" Run");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(readFile() == this.age){
//            System.out.println("Dad: "+this.name+ " " + this.age  );
            logger.info("Dad: "+this.name+ " " + this.age );
            shareData.sumCount();
//            System.out.println("Dad tăng count "+ shareData.getName() + " lên " + shareData.getCount());
            logger.info("Dad tăng count "+ shareData.getName() + " lên " + shareData.getCount());
        }

        Bt2Application.countEndThread ++ ;
//        System.out.println("DadThread " + shareData.getName() + " Stop");
        logger.info("DadThread " + shareData.getName() + " Stop");
    }

    public int readFile() {
        int age = 0 ;
        String url = "bt2/src/main/resources/static/text/dad.txt";

        // Đọc dữ liệu từ File với BufferedReader
        FileInputStream fileInputStream = null;
        BufferedReader bufferedReader = null;

        try {
            fileInputStream = new FileInputStream(url);
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] res = line.split(" ") ;
                String test = res[0] .toLowerCase() ;
                if(test.equals(name.toLowerCase()) ) {
                    age = Integer.valueOf(res[1]);
                }
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                bufferedReader.close();
                fileInputStream.close();
            } catch (IOException ex) {

            }
        }

        return age ;
    }


}