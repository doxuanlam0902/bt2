package com.example.bt2.Thread;

import com.example.bt2.model.ShareData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ListenerThread extends Thread{

    public ShareData shareData;
    public List<Thread> myListThreads ;

//    private final CountDownLatch latch;

    public ListenerThread(ShareData shareData){
        this.shareData = shareData ;
        this.myListThreads = new ArrayList<>() ;
    }

    @Override
    public void run(){
        System.out.println("Listener Starting");
        for(int i = 0 ; i< myListThreads.size() ; i++){
            myListThreads.get(i).start();
        }
        while(shareData.getCount()<2){
            for(int i = 0 ; i< myListThreads.size() ; i++){
                if(i == myListThreads.size()-1 && !myListThreads.get(i).isAlive()){
                    return;
                }
                if(myListThreads.get(i).isAlive()){
                    break ;
                }
            }
        }
        for(int i = 0 ; i< myListThreads.size() ; i++){
            if(myListThreads.get(i).isAlive()){
                myListThreads.get(i).stop();
            }
        }
        System.out.println("Listener End");
    }


}
