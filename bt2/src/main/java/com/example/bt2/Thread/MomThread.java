package com.example.bt2.Thread;


import com.example.bt2.Bt2Application;
import com.example.bt2.model.ShareData;
import com.example.bt2.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;

public class MomThread extends Thread {

    private final Logger logger = LoggerFactory.getLogger(TestService.class);


    private String name ;
    private int age ;

    private ShareData shareData ;

    public MomThread(String name , int age, ShareData shareData) {
        this.name = name ;
        this.age = age ;
        this.shareData = shareData ;
    }

    @Override
    public void run() {
//        System.out.println("MomThread "+shareData.getName()+" Run");
        logger.info("MomThread "+shareData.getName()+" Run");
        try {
            Thread.sleep(12000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(readFile() == this.age){
//            System.out.println("Mom :" +this.name + " "+ this.age+ " tuổi" );
            logger.info("Mom :" +this.name + " "+ this.age+ " tuổi" );
            shareData.sumCount();
//            System.out.println("Mom tăng count "+ shareData.getName() +" lên" + shareData.getCount());
            logger.info("Mom tăng count "+ shareData.getName() +" lên" + shareData.getCount());

        }
        Bt2Application.countEndThread ++ ;
//        System.out.println("MomThread " +shareData.getName()+ " Stop");
        logger.info("MomThread " +shareData.getName()+ " Stop");
    }

    public int readFile(){
        int age = 0 ;
        String url = "bt2/src/main/resources/static/text/mom.txt";

        // Đọc dữ liệu từ File với BufferedReader
        FileInputStream fileInputStream = null;
        BufferedReader bufferedReader = null;

        try {
            fileInputStream = new FileInputStream(url);
            bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] res = line.split(" ") ;
                String test = res[0] .toLowerCase() ;
                if(test.equals( name.toLowerCase())) {
//                    System.out.println("Mom " + res[0]);
                    int year = Integer.valueOf(res[1]);
                    Calendar instance = Calendar.getInstance();
                    int yearNow = instance.get(Calendar.YEAR);
                    age = yearNow - year ;
                }
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                bufferedReader.close();
                fileInputStream.close();
            } catch (IOException ex) {

            }
        }
        return age ;
    }
}
