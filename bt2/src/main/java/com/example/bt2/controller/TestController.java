package com.example.bt2.controller;


import com.example.bt2.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class TestController {

    @Autowired
    public TestService testService ;

    private final Logger logger = LoggerFactory.getLogger(TestService.class);

    @GetMapping("/getRes")
    public boolean getRes(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "age") int age ){

        logger.info("Inside getRes of TestController");
        return testService.checkRes(name , age) ;
    }
}
