package com.example.bt2.model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public  class ShareData {
    private BlockingQueue<Integer> myBlockingQueue = new ArrayBlockingQueue<Integer>(2) ;
    private int count  = 0 ;
    private String name  ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShareData(String name) {
        this.name = name ;
    }

    public synchronized void sumCount(){
        this.count ++ ;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
