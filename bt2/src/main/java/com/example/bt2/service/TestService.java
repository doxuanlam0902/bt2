package com.example.bt2.service;

import com.example.bt2.Thread.DadThread;
import com.example.bt2.Thread.ListenerThread;
import com.example.bt2.Thread.MomThread;
import com.example.bt2.Thread.UBNDThread;
import com.example.bt2.model.ShareData;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import static com.example.bt2.Bt2Application.count;
import static com.example.bt2.Bt2Application.countEndThread;

@Service
@Slf4j
public class TestService {

    private final Logger logger = LoggerFactory.getLogger(TestService.class);

    public boolean checkRes(String name , int age){
        logger.info("Inside checkRes of TestService");

        boolean test = false ;
        ShareData shareData = new ShareData(name) ;

        ListenerThread listenerThread = new ListenerThread(shareData) ;

        DadThread t1 = new DadThread(name.toLowerCase(),age ,shareData) ;
        MomThread t2 = new MomThread(name.toLowerCase(),age , shareData) ;
        UBNDThread t3 = new UBNDThread(name.toLowerCase(),age , shareData);


        listenerThread.myListThreads.add(t1) ;
        listenerThread.myListThreads.add(t2) ;
        listenerThread.myListThreads.add(t3) ;

        listenerThread.start();
        try {
            listenerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(shareData.getCount() >= 2){
            return true ;
        }else {
            return false ;
        }


//        synchronized (ShareData.class){
//            while(t1.isAlive() || t2.isAlive() || t3.isAlive()){
//                if(shareData.getCount() >= 2) {
//                    if(t1.isAlive()){
//                        t1.stop();
////                        System.out.println("DadThread "+ shareData.getName() +" Stop");
//                        logger.info("DadThread "+ shareData.getName() +" Stop");
//                    }
//                    if(t2.isAlive()){
//                        t2.stop();
////                        System.out.println("MomThread "+ shareData.getName() + " Stop");
//                        logger.info("MomThread "+ shareData.getName() + " Stop");
//                    }
//                    if(t3.isAlive()){
//                        t3.stop();
////                        System.out.println("UBNDThread "+ shareData.getName() + " Stop");
//                        logger.info("UBNDThread "+ shareData.getName() + " Stop");
//                    }
//                    return true;
//                }else{
//                    continue ;
//                }
//            }
//        }
//        if(shareData.getCount() >= 2) {
//            return true ;
//        }
//        if(t1.isAlive()){
//            t1.stop();
//        }
//        if(t2.isAlive()){
//            t2.stop();
//        }
//        if(t3.isAlive()){
//            t3.stop();
//        }

//        return false ;
    }
}
